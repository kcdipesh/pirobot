var app = require('express')(),
    http =  require('http').Server(app),
    io = require('socket.io')(http),
    Gpio = require('onoff').Gpio,
    leftMotor1 = new Gpio(11,'out'),
    leftMotor2 = new Gpio(12,'out'),
    rightMotor1 = new Gpio(15,'out'),
    rightMotor2 = new Gpio(16,'out')
;

app.get('/', function(req,res) {
  console.log('yo');
});


http.listen(3000,function(){
  console.log('now listening on *.3000');
});

function goForward(){
    leftMotor1.writeSync(1);
    leftMotor2.writeSync(0);
   
    rightMotor1.writeSync(1);
    rightMotor2.writeSync(0);
}

function stop(mode){
    //mode = parseInt(mode,10) || 0;
    //default mode is 0, ie soft stop
    leftMotor1.writeSync(mode);
    leftMotor2.writeSync(mode);
   
    rightMotor1.writeSync(mode);
    rightMotor2.writeSync(mode);

}
function leftMotor(mode) {
    if (mode===0){
        // 0 = stop
        leftMotor1.writeSync(0);
        leftMotor2.writeSync(0);

    }
    else if (mode === 1) {
        // 1= forward
        leftMotor1.writeSync(1);
        leftMotor2.writeSync(0);        
    }
    else if (mode === 2){
        // 2 = reverse
        leftMotor1.writeSync(0);
        leftMotor2.writeSync(1);
    }else {
        // -force stop
        leftMotor1.writeSync(1);
        leftMotor2.writeSync(1);
    }
}

function rightMotor(mode) {
    if (mode===0){
        // 0 = stop
        rightMotor1.writeSync(0);
        rightMotor2.writeSync(0);

    }
    else if (mode === 1) {
        // 1= forward
        rightMotor1.writeSync(1);
        rightMotor2.writeSync(0);        
    }
    else if (mode === 2){
        // 2 = reverse
        rightMotor1.writeSync(0);
        rightMotor2.writeSync(1);
    }else {
        // -force stop
        rightMotor1.writeSync(1);
        rightMotor2.writeSync(1);
    }
}



function stop(){
  
}

io.on('connection', function(socket){
	console.log('remote connected!');
	
	//forward event received
	socket.on('f',function(){
        leftMotor(1);
        rightMotor(1);
		console.log('forward');
	});
	
    socket.on('b',function(){
        leftMotor(2);
        rightMotor(2);
        console.log('backward');
    });

    socket.on('l',function(){
        leftMotor(0);
        rightMotor(1);
        console.log('left');
    });

    socket.on('r',function(){
        leftMotor(1);
        rightMotor(0);
        console.log('right');
    });

    //soft stop evetn
    socket.on('ss',function(){
        leftMotor(0);
        rightMotor(0);
        console.log('soft stop');

    });    

    //hard stop evetn
    socket.on('hs',function(){
        leftMotor(3);
        rightMotor(3);
        console.log('soft stop');

    });

	//forward left
	// socket.on('fl',function(){
	// 	leftMotor(0);
 //        rightMotor(0);
 //        console.log('soft stop');

	// });


	socket.broadcast.emit('feedback',{});

});


